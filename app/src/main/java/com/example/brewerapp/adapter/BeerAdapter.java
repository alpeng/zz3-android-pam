package com.example.brewerapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.brewerapp.R;
import com.example.brewerapp.model.Beer;
import com.example.brewerapp.utils.OnBeerClickedListener;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class BeerAdapter extends RecyclerView.Adapter<BeerAdapter.ViewHolder> {

    /*  --  ViewHolder  ----------------------------------------------- */

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView beerLabel;

        public ViewHolder(View v) {
            super(v);
            beerLabel = v.findViewById(R.id.beer_name);
        }
    }

    /*  --  Attributes  ----------------------------------------------- */

    private List<Beer> beers;
    private OnBeerClickedListener listener;

    /*  --  Constructors  --------------------------------------------- */

    public BeerAdapter(List<Beer> beers, OnBeerClickedListener listener) {
        this.beers = beers;
        this.listener = listener;
    }

    /*  --  Setters  -------------------------------------------------- */

    public void setBeers(List<Beer> b) {
        if (beers == null) {
            beers = b;
        } else {
            this.beers.clear();
            this.beers.addAll(b);
        }
        // Refresh data display
        notifyDataSetChanged();
    }

    /*  --  Super Methods  -------------------------------------------- */

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Create and inflate the layout
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View beerView = inflater.inflate(R.layout.item_beer, parent, false);

        // Create the view holder
        return new ViewHolder(beerView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Beer beer = beers.get(position);

        holder.beerLabel.setText(beer.name);
        holder.itemView.setOnClickListener(v -> listener.onBeerClicked(beer));
    }

    @Override
    public void onViewRecycled(@NotNull ViewHolder holder) {
        super.onViewRecycled(holder);
    }

    @Override
    public int getItemCount() {
        return beers == null ? 0 : beers.size();
    }
}
