package com.example.brewerapp.model;

import android.util.Log;

import org.jetbrains.annotations.NotNull;

public class Beer {

    /*  --  Attributes  ----------------------------------------------- */

    public int id;
    public String name;
    public String cat_name;
    public String name_breweries;
    public String style_name;

    /*  --  Constructor  ---------------------------------------------- */

    public Beer(int id) {
        this.id = id;
        this.name = "Beer " + id;
        Log.i("Beer", toString());
    }

    /*  --  Methods  -------------------------------------------------- */

    @NotNull
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("id:").append(id).append("\t")
          .append("name:").append(name).append("\t")
          .append("cat_name:").append(cat_name).append("\t")
          .append("name_breweries:").append(name_breweries).append("\t")
          .append("style_name:").append(style_name);

        return sb.toString();
    }
}
