package com.example.brewerapp.model;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.brewerapp.rest.APICommunicator;
import com.example.brewerapp.utils.OnDataLoadedListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BeerViewModel extends ViewModel {

    /*  --  Attributes  ----------------------------------------------- */

    private MutableLiveData<List<Beer>> beersLiveData;

    /*  --  Constructors  --------------------------------------------- */

    public BeerViewModel() {
        beersLiveData = new MutableLiveData<>();
    }

    /*  --  Getters  -------------------------------------------------- */

    public LiveData<List<Beer>> getLiveData() {
        return beersLiveData;
    }

    public List<Beer> getBeers() {
        return getLiveData().getValue();
    }

    /*  --  Methods  -------------------------------------------------- */

    public void loadBeers(OnDataLoadedListener listener) {
        Call<BeerAPIResult> APIResult = APICommunicator.getInstance().getAPI().getDefaultBeers();

        APIResult.enqueue(new Callback<BeerAPIResult>() {

            @Override
            public void onResponse(@NotNull Call<BeerAPIResult> call, @NotNull Response<BeerAPIResult> response) {
                if (response.isSuccessful() && response.body() != null) {
                    List<Beer> beerList = new ArrayList<>();
                    response.body().records.forEach(metaBeer -> { beerList.add(metaBeer.fields); });
                    Log.i("fetch size", beerList.size() + " elements");
                    beersLiveData.setValue(beerList);
                    listener.onDataLoaded();
                } else {
                    Log.e("fetch data", Objects.requireNonNull(response.errorBody()).toString());
                }
            }

            @Override
            public void onFailure(@NotNull Call<BeerAPIResult> call, @NotNull Throwable t) {
                Log.e("Network call", t.getMessage(), t);
            }
        });
    }
}
