package com.example.brewerapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;

import com.example.brewerapp.adapter.BeerAdapter;
import com.example.brewerapp.fragment.BeerListFragment;
import com.example.brewerapp.model.BeerViewModel;
import com.example.brewerapp.R;
import com.example.brewerapp.utils.OnDataLoadedListener;

/*
    Cours Android

    Alexandre Peng
    Sylvain Bourgea

 */


public class MainActivity extends AppCompatActivity {

    /*  --  Attributes  ----------------------------------------------- */

    // UI attributes
    private RecyclerView beer_RecyclerView;
    private BeerAdapter  beer_Adapter;
    private RecyclerView.LayoutManager beer_LayoutManager;
    private BeerListFragment detailFragment;

    // Data attributes
    private BeerViewModel beerVM;

    /*  --  Super Methods  -------------------------------------------- */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Internet permission
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED) {
            Log.e("Permission check", "Internet not granted");
        } else {
            Log.i("Permission check", "Internet granted");
        }

        beerVM = new ViewModelProvider(this).get(BeerViewModel.class);

        beer_RecyclerView = findViewById(R.id.rvBeers);

        beer_LayoutManager = new LinearLayoutManager(this);
        beer_RecyclerView.setLayoutManager(beer_LayoutManager);

        detailFragment = new BeerListFragment();
        beer_Adapter = new BeerAdapter(beerVM.getBeers(), detailFragment);
        beer_RecyclerView.setAdapter(beer_Adapter);

        // Refresh the screen after data are fetched
        beerVM.loadBeers(new OnDataLoadedListener() {
            @Override
            public void onDataLoaded() {
                beer_Adapter.setBeers(beerVM.getBeers());
            }
        });

        // Manage the fragments
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.rvBeers, detailFragment);
        }
    }
}
