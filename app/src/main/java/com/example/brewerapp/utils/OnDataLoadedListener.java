package com.example.brewerapp.utils;

public interface OnDataLoadedListener {
    public void onDataLoaded();
}
