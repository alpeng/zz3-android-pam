package com.example.brewerapp.utils;

import com.example.brewerapp.model.Beer;

public interface OnBeerClickedListener {
    void onBeerClicked(Beer b);
}
