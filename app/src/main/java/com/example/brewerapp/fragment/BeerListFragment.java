package com.example.brewerapp.fragment;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.brewerapp.R;
import com.example.brewerapp.model.Beer;
import com.example.brewerapp.utils.OnBeerClickedListener;

public class BeerListFragment extends Fragment implements OnBeerClickedListener, View.OnTouchListener {

    /*  --  Attributes  ----------------------------------------------- */

    private Beer beer;

    private TextView beerName;
    private TextView brewerName;

    private LinearLayout linearLayout;

    /*  --  Constructor  ---------------------------------------------- */

    public BeerListFragment() {}

    /*  --  Methods  -------------------------------------------------- */

    private void displayData() {
        beerName.setText(beer.name);
        brewerName.setText(beer.name_breweries);
        linearLayout.setVisibility(View.VISIBLE);
    }

    /*  --  Super Methods  -------------------------------------------- */

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_beer, container, false);
        beerName = view.findViewById(R.id.beer_name);
        brewerName = view.findViewById(R.id.brewer_name);
        linearLayout = view.findViewById(R.id.beer_data);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        displayData();
    }

    @Override
    public void onBeerClicked(Beer b) {
        beer = b;
        Log.i("Beer listener", b.name + " clicked");
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                v.getBackground().setColorFilter(0x0022003, PorterDuff.Mode.SRC_ATOP);
                v.invalidate();
                break;
            }
            case MotionEvent.ACTION_UP: {
                v.getBackground().clearColorFilter();
                v.invalidate();
                break;
            }
        }
        return false;
    }
}
