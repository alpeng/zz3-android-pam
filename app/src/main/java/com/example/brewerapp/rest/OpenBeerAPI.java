package com.example.brewerapp.rest;

import com.example.brewerapp.model.BeerAPIResult;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface OpenBeerAPI {

    // Default call
    @GET("?dataset=open-beer-database%40public-us")
    Call<BeerAPIResult> getDefaultBeers();

    @GET("?dataset=open-beer-database%40public-us&row={row}")
    Call<BeerAPIResult> getBeers(@Path("row") int row);
}
