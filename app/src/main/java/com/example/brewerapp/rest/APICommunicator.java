package com.example.brewerapp.rest;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APICommunicator {

    /*  --  Attributes  ----------------------------------------------- */

    private                 OpenBeerAPI     api;
    private static volatile APICommunicator instance;

    /*  --  Constructors  --------------------------------------------- */

    private APICommunicator() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://data.opendatasoft.com/api/records/1.0/search/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        api = retrofit.create(OpenBeerAPI.class);
    }

    /*  --  Getters  -------------------------------------------------- */

    public OpenBeerAPI getAPI() {
        return api;
    }

    public static synchronized APICommunicator getInstance() {
        if (instance == null) { instance = new APICommunicator(); }
        return instance;
    }
}
